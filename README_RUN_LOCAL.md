[<- Volver](README.md)

## Localhost:

## 1) Importar proyecto clonado

Importar proyecto Maven > a excepción del proyecto script, elegir todos los proyectos del repositorio emir2 clonado en local e importarlos desde eclipse. 

* No adjuntar proyecto script

![img](misc/proyectos_local.png)


Aguardar a que se vayan actualizando y sincronizando. 

Algunos warnings y errores de pom quedaran visibles, esto no afecta al despliegue en local. 

Corremos el proyecto SOA-QA y VIEW-QA.

![img](misc/img-soa-view.png)

URL local 

http://localhost:8666/sim → usuario

http://localhost:8666/sim/pp → usuario producción 


## 2) Configurar Clase Tablas --> package mir.util.test.pp;

* [CLASE_TABLAS](README_SIM_COPI_TEST.md)


## 3) Configurar - TestNG QA

* [TEST_NG_LOCAL_QA](README_TEST_NG_QA.md)


## 4) Configurar - TestNG COPIA

* [TEST_NG_LOCAL_COPIA](README_TEST_NG_COPIA.md)