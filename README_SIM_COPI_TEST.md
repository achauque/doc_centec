[<- Volver](README.md)

## Package mir.util.test.pp - Class Tablas

Algunas configuraciones previas a llenar las bases de datos.

Eclipse > Buscar: ctrl + shift + T = Tablas.java

Esta clase Tablas contiene la conexión local y real a bases de datos. Des comentar los métodos que se van a utilizar.

Configuración para Usuario SIMCOPI y SIMTEST


```console

package mir.util.test.pp;

public final class Tablas {


// TODO Se debe crear y utilizar a otro usuario distinto al usuario REAL
// "SIMPROD". Para evitar llegar a bloquar usuario por error. !!!

//DB RODUCCION BUENOS AIRES
private static String DB_SIM_REAL_BA="##jdbc:oracle:thin:@srvdc1or01.ad.mirgor.com.ar:1521:SIM";
//private static String DB_SIM_REAL_BA_NULL=null;

//DB RODUCCION RG
private static String DB_SIM_REAL_RG="##jdbc:oracle:thin:@simdbrg.ad.mirgor.com.ar:1521/SIM";
//	private static String DB_SIM_REAL_RG_NULL=null;


//QA 
private static String DB_SIM_QA="jdbc:oracle:thin:@vmrg1oraqa1.ad.mirgor.com.ar:1521:SIM";
//	private static String DB_SIM_QA_NULL=null;


//LOCAL SIM COPI
private static String DB_SIM_LOCAL="jdbc:oracle:thin:@localhost:1521/XE";

public static String[] CONEXION_LOCAL   = new String[] { DB_SIM_LOCAL, "SIMTEST", "mir2pw" };
public static String[] CONEXION_REAL_BA = new String[] { DB_SIM_REAL_BA, "", "" };
public static String[] CONEXION_REAL    = new String[] { DB_SIM_REAL_RG, "", "" };
public static String[] CONEXION_COPIA   = new String[] { DB_SIM_LOCAL, "SIMCOPI", "SIMCOPI01" };
public static String[] CONEXION_QA      = new String[] { DB_SIM_QA, "SIMQA", "SIMQA01" };
}

```