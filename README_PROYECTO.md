[<- Volver](README.md)

## Linux:

Para el correcto funcionamiento del proyecto se deben realizar configuraciones adicionales sobre las herramientas instaladas en elcipse, sobre todo en las versiones que utilizara java y groovy.

Estas configuraciones son accedidas del menu de preferencias:

![MENU_PREFERENCES_1](misc/menu_preferences.png)



## Java 1.8:

Durante el proceso de instalación de eclipse es probable que se haya descargado una versión superior > 1.8 de java motivo por el cual hay que especificar que la herramienta debe trabajar con la version 1.8:

* Java compiler:

Se necesita seleccionar la version 1.8 del compilador

![JAVA_CONF_0](misc/java_conf_0.png)

* JRE:

Es necesario por la ruta al directorio donde se instalo JDK.

![JAVA_CONF_1](misc/java_conf_1.png)

![JAVA_CONF_1](misc/java_conf_2.png)


## Groovy:

Intercambio en la versión por defecto de groovy. Se requiere seleccionar la versio 2.5.~:

![GROOVY_CONF_1](misc/groovy_conf_0.png)


## Maven:

En maven debemos verificar que se respeten los directorios por defecto, que seran util mas adelante:

![MAVEN_CONF_0](misc/maven_conf_0.png)


## Clonamos el proyecto:

Debemos clonar el proyecto desde el repositorio de git en la ubicación asignada para el work-space de eclipse. Los siguientes pasos deben realizarse con la aplicación eclipse cerrada:


```console
user@host:~/eclipse-workspace$ git clone http://git.ad.mirgor.com.ar/emir2/emir2.git
```


## Construimos el proyecto:


En la carpeta build del proyecto vamos ejecutar el comando mvn para poder construir parte del mismo. En primer instancia obtendremos un error en "mir util", pero no es relevante.
En esta instancia hay que recordar estar conectado a la VPN ya que maven ira a buscar a repositorios las dependencias necesarias:


```console
user@host:~/eclipse-workspace/emir2/build$ mvn clean compile -DskipTests
```

![MVN_ERROR](misc/mvn_error.png)


Para solucionar el error de dependencia mencionado en el paso anterior debemos descargar [SNAPSHOT](https://github.com/osinagalj/MirgorFiles) y colocarlo en la carpeta de repositorios de maven.

Una vez descargado y descomprimido el archivo debemos copiar el directorio completo "3.3.0-SNAPSHOT" en al ruta por defecto para el repositorio de maven, en este caso:

* /home/usuario/.m2/repository/io/vertx/vertx-ignite

En caso de que no exita la carpeta "vertx-ignite" debemos crearla.

En el mismo repositorio donde descargamos [SNAPSHOT](https://github.com/osinagalj/MirgorFiles) encontraremos un archivo llamado "settings.xml" el cual debemos descargar y copiar en la ruta del repositorio:

* /home/usuario/.m2/

Una vez copiado necesitamos editar el path para el repositorio:

* antes:
    ```xml
	<localRepository>/home/lautaro/desarrollo/maven/repo</localRepository>
    ```

* despues:
    ```xml
	<localRepository>/home/usuario/.m2/repository</localRepository>
    ```

recordar reemplazar la palabra usuario por el nombre de usuario correspondiente

Una vez copiado SNAPSHOT y editado settings.xml procedemos a volver a construir el proyecto:

```console
user@host:~/eclipse-workspace/emir2/build$ mvn clean -U compile -DskipTests
```

Si todo salio bien esta vez deberia terminar de construir el proyecto satisfactoriamente:

![MVN_SUCCESS](misc/mvn_success.png)
