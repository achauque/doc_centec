[<- Volver](README.md)

## Linux:

En una instalación fresca de Ubuntu no habra problema con este comando. En caso contrario solo se debe verificar que no se esten usando los puertos 8080 y 1521.

Tener en cuenta que este comando puede tardar algunos minutos dependiendo de la conexión y hardware. Ya que descarga el contenedor y lo despliega.

Descargamos imagen del contenedor:

```console
user@host:~$ docker pull quay.io/maksymbilenko/oracle-12c
```

Desplegamos el contenedor:

```console
user@host:~$ docker run -d -p 8080:8080 -p 1521:1521 quay.io/maksymbilenko/oracle-12c
```

## Windows:

## Mac: