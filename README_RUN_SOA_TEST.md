[<- Volver](README.md)

Accediendo al punto de menu **Run** de eclipse podremos acceder al sub-menu **Run Configurations...**.

![Mirgor](misc/run_new.png)


Main:
``` 
Name: SOA-TEST
Project: soa
Main Class: mir.soa.SOAApplication
```


VM Arguments:
``` 
-DLOG_PATH=/tmp/log/ -Xms512m -Xmx1G -Duser.country=AR -Duser.language=es
-Dserver.context-path=/soa
-server
-Dsim.simulaLogin=true
-Dspring.profiles.active=test
-Dserver.port=8180
-Dtomcat.ajp.port=8109
-Dsim.portEB=8000
-Dspring.config.location=/{ruta al proyecto}/emir2/env/properties/
``` 