[<- Volver](README.md)

## Linux:

Eliminamos cualquier versión previa de Docker:

```console
user@host:~$ sudo apt-get remove docker docker-engine docker.io containerd runc
```

Atualizamos el indice de paquetes de apt e instalamos paquetes para permitir que apt user repos sobre HTTPS:

```console
user@host:~$ sudo apt-get update
```

```console
user@host:~$ sudo apt-get install ca-certificates curl gnupg lsb-release
```

Agregamos la llave PGP oficial de Docker:

```console
user@host:~$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```

Agregamos el repositorio **estable** de Docker:

```console
user@host:~$ echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

Instalamos Docker Engine:

```console
user@host:~$ sudo apt-get update
```

```console
user@host:~$ sudo apt-get install docker-ce docker-ce-cli containerd.io
```

Creamos el grupo docker y agregamos el usuario iniciado al grupo. Esto sirve para evitar el error de falta de privilegios al usuario iniciado:

```console
user@host:~$ sudo groupadd docker
```

```console
user@host:~$ sudo gpasswd -a $USER docker
```

Es recomendable re-iniciar el equipo luego de llevar a cabo la instalación de Docker.

Referencia:

https://docs.docker.com/engine/install/ubuntu/

## Windows:

### Configuración de Vmmem para que no consuma recursos en Docker (WSL)

Esto se aplica a WSL2. Si está ejecutando WSL1, probablemente no debería hacerlo.

#### **Cerrar WSL**

En primer lugar, queremos deshacernos de WSL para liberar algunos recursos y
asegurarnos de que recoja nuestros cambios de configuración más adelante.

Ejecute esto en su consola de comando:

```console
wsl --shutdown
```

#### **Encuentre su archivo .wslconfig**

Navegue a **%UserProfile%** (simplemente pegue ese texto tal como está en la
barra de direcciones del Explorador de Windows) y vea si tiene un archivo
**.wslconfig** disponible (asegurarse de tener los archivos ocultos, visibles).

O puede intentar agregar **%UserProfile% /.wslconfig** en un editor de texto
directamente. Si aún no existe, debe crearlo.

#### **Edite su archivo .wslconfig**

Agregar la siguiente línea al archivo

```console
[wsl2]
```

- **Limitar el uso de memoria:** agregar la siguiente línea debajo.

```console
memory=[cantidad GB]

Ej: memory=2GB
```

- **Limitar la cantidad de nucleos del procesador:** agregar la siguiente línea debajo.

```console
processors=[cantidad]

Ej: processors=4
```

#### **Reiniciar WSL**

Si estaba usando Docker, debe reiniciarlo. Si tenía alguna otra cosa vinculada, simplemente inicie WSL.
¡Y listo, debería tener lista su nueva configuración!

## Mac:
