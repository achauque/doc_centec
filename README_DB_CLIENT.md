[<- Volver](README.md)

Para trabajar con los datos de la aplicación es necesario contar con un cliente para conectarse a Oracle &copy;

En este paso se pueden recomendar dos clientes:

DBeaver Community Edition : https://dbeaver.io/download/

SQL Workbench/J : https://www.sql-workbench.eu/downloads.html


## Linux:

* DBeaver : 
    
    El nombre del archivo *.deb dependera de la versión descargada en el momento de revisar este manual

    ```console
    user@host:~/Descargas$ sudo dpkg -i dbeaver-ce_22.0.1_amd64.deb
    ```

* SQL Workbench/J :

    Al momento de escribir este manual el release 127 es el ultimo que puede ejecutarse con Java 1.8

    Una vez descomprimido el archivo podremos ejecutar el archivo "sqlworkbench.sh" contenido.

    Hay que tener en cuent que para este cliente se necesita descrgar ojdbc6.jar 

    ```console
    user@host:~/Descargas/Workbench-Build127$ ./sqlworkbench.sh
    ```

## Windows:

## Mac: