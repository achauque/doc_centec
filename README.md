![Mirgor](misc/logo.png)

## SIM 
**S**istema **I**ndustrial **M**irgor &copy;

El sistema denominado SIM, es un conjunto de artefactos de sofware que forman parte del piso de planta (MES) para la empresa Mirgor.

[Documento de introducción a SIM](misc/SIM_Conceptos_Generales_v2.pdf)

## Requisitos:


* [install](README_JAVA.md) JAVA 1.8 (oracle)
* [install](README_MAVEN.md) Maven ~3.6
* [install](README_GROOVY.md) Groovy 2.5
* [install](README_DOCKER.md) Docker
* [install](README_ORACLE.md) DB Oracle 12 sobre Docker
* [install](README_ACTIVEMQ.md) ActiveMQ sobre Docker
* [install](README_ECLIPSE.md) Eclipse
* [install](README_FORTICLIENT.md) Forticlient y config VPN
<br><br>


**Obesrvaciones:**
Los procesos de instalación que se detallaron fueron realizados sobre sistemas operativos recien instalados; 
y por tal motivo pueden cambiar en su entorno.

## Configurar proyecto en eclipse:

* [Configuracion](README_PROYECTO.md)  de entorno eclipse

## BD ambiente local:

* [CLIENTE BD SQL](README_DB_CLIENT.md)
* [CONEXIONES Y USUARIOS BASE DE DATOS](README_DB_USERS.md)
* SIMCOPI
* SIMTEST


## RUN Configurations:
* [SOA-QA](README_RUN_SOA_QA.md) 
* [VIEW-QA](README_RUN_VIEW_QA.md) 
* [SOA-TEST](README_RUN_SOA_TEST.md) 
* [VIEW-TEST](README_RUN_VIEW_TEST.md) 


## Estructura de BD:

* [Indice](README_TABLAS.md) de tablas


## Clonar proyecto:
```console
user@host:~$ git clone http://git.ad.mirgor.com.ar/emir2/emir2.git
```

## RUN Localhost:

* [LOCAL](README_RUN_LOCAL.md)

