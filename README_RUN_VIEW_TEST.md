[<- Volver](README.md)

Accediendo al punto de menu **Run** de eclipse podremos acceder al sub-menu **Run Configurations...**.

![Mirgor](misc/run_new.png)


Main:
```
Name: VIEW-TEST
Project: view-vertx
Main Class: mir.view.Application
``` 


VM Arguments:
``` 
-DLOG_PATH=/tmp/log/ -Xms512M -Xmx2G
-server
-Duser.country=AR -Duser.language=es
-Dspring.profiles.active=test
-Dspring.config.location=/{ruta al proyecto}/emir2/env/properties/
-Dserver.port=8666
-Dsim.portEB=0
-Dview-path=/sim
-Dsim.simulaLogin=true
-DsimulaLogin=true
``` 