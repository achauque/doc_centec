[<- Volver](README.md)

## Linux:
Requisitos: Ubuntu 20.04 LTS

Paso 1 Descargar JDK:

El primer paso consiste en descargar la versión comprimida de [JDK 1.8](https://www.oracle.com/java/technologies/downloads/#java8-linux) desde la pagina oficial de Oracle

![Download](misc/oracle-java-download.png)

Paso 2 Descomprimir JDK:

* Crear directorio /opt/jdk :

    ```console
    user@host:~$ sudo mkdir /opt/jdk
    ```

* Descomprimir JDK:

    El nombre de archivo jdk-8u321-linux-x64.tar.gz puede cambiar según el release liberado por Oracle&copy;.

    Dirijase al directorio donde se realizo la descarga (Descargas):

    ```console
    user@host:~/Descargas$ sudo tar -zxf jdk-8u321-linux-x64.tar.gz -C /opt/jdk/
    ```

Paso 3 Instalar JDK usando *Alternatives*:

El nombre de la capeta jdk1.8.0_311 puede cambiar según el release liberado por Oracle&copy;.

```console
user@host:~$ sudo update-alternatives --install /usr/bin/java java /opt/jdk/jdk1.8.0_311/bin/java 100
```

Resultado de la ejecución del comando: 

```console
user@host:~$ update-alternatives: using /opt/jdk/jdk1.8.0_311/bin/java to provide /usr/bin/java (java) in auto mode
```

En caso de poseer mas de una instancia de JDK podrá seleccionar entre ellas usando *Alternatives*:

```console
user@host:~$ sudo update-alternatives --config java
```

Resultado de la ejecución del comando: 

```console
user@host:~$
Existen 3 opciones para la alternativa java (que provee /usr/bin/java).

  Selección   Ruta                                         Prioridad  Estado
------------------------------------------------------------
  0            /usr/lib/jvm/java-17-openjdk-amd64/bin/java   1711      modo automático
* 1            /opt/jdk/jdk1.8.0_311/bin/java                100       modo manual
  2            /usr/lib/jvm/java-11-openjdk-amd64/bin/java   1111      modo manual
  3            /usr/lib/jvm/java-17-openjdk-amd64/bin/java   1711      modo manual

Pulse <Intro> para mantener el valor por omisión [*] o pulse un número de selección: 
```

Paso 4 Setear variables de entorno:

Se necesita agregar dos variables de entorno JAVA_HOME y JRE_HOME al archivo *environment* a continuacion del contenido existente del mismo:

```console
user@host:~$ sudo nano /etc/environment
```

> PATH="/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/>

> JAVA_HOME=/opt/jdk/jdk1.8.0_311

> JRE_HOME=/opt/jdk/jdk1.8.0_311/jre


Chequear variables definidas:
```console
user@host:~$ source /etc/environment
user@host:~$ echo $JAVA_HOME
```
Resultado de la ejecución del comando: 

```console
user@host:~$ /opt/jdk/jdk1.8.0_251
```

```console
user@host:~$ sudo apt-get update
```

Paso 5 Verificar versión:

```console
user@host:~$ java -version
```

Resultado de la ejecución del comando: 

```console
user@host:~$ 
java version "1.8.0_311"
Java(TM) SE Runtime Environment (build 1.8.0_311-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.311-b11, mixed mode)
```




## Windows:

## Mac: