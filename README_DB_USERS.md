[<- Volver](README.md)

Para todos estos pasos es necesario tener ejecutado el docker de Oracle &copy;

Vamos a configurar diversas conexiones y ejecutar algunos script necesarios para terminar de montar el ambiente TEST y conexiones
basicas contra el ambiente QA (que reside en Mirgor).

Para estas tareas vamos valernos de cualquier de los dos clientes de base de datos sugeridos (dbeaver, SQL Workbench/J)


### CONEXION SYSTEM:


La conexión con el usuario por defecto **system** desplegado con docker nos permitira luego ejecutar 2 script para poder generar los usuarios SIMCOPI y SIMTEST.

SIMCOPI: es un ambiente intermedio donde se traen las tablas y datos necesarios desde el ambiente QA para mantener una copia desde donde se puede restaurar con mayor celeridad en caso de ser necesario el ambiente TEST

TEST: es el ambiente donde apunta realmente el proyecto al momento de ser ejecutado.


```
host: localhost
port: 1521
DataBase: XE
user: system
pass: oracle
```

![DBEAVER_NEW_CONN](misc/dbeaver_new_conn.png)
![DBEAVER_CONF_CONN](misc/dbeaver_conf_conn.png)



### SCRIPT PARA CREAR AMBIENTE SIMTEST:

``` sql
CREATE USER SIMCOPI IDENTIFIED BY SIMCOPI01;
GRANT CREATE SESSION TO SIMCOPI;
GRANT ALL PRIVILEGES TO SIMCOPI;
```
### SCRIPT PARA CREAR AMBIENTE SIMCOPI: 

``` sql
CREATE USER SIMTEST IDENTIFIED BY mir2pw;
GRANT CREATE SESSION TO SIMTEST;
GRANT ALL PRIVILEGES TO SIMTEST;
```


### CONEXION SIMTEST:

```
host: localhost
port: 1521
DataBase: XE
user: SIMTEST
pass: mir2pw
```

![DBEAVER_CONF_SIMTEST](misc/dbeaver_conf_simtest.png)