[<- Volver](README.md)


| Tabla | Clase | Contenido |
|---|---|---|
| INVM20M | FormatoEtiquetaDeImpresion |  |
| INVM22M | Recurso |  |
| INVM23M | RelacionCentro |  |
| INVM24M | DefinicionLenguaje |  |
| INVM25M | Lenguaje |  |
| INVM26M | Serie |  |
| INVM27M | RelacionSerie |  |
| INVM28M | Tracking |  |
| MM023 | ValorAtributo | Contiene categorias. (Puestos PAF, etc) |
| MM026 | ModeloPuesto | Relaciona los modelos a sus diferentes puestos |
| MM027 | ImagenModeloPuesto | Relaciona el modelo-puesto (MM026) con una imagen |
| PP001 | OrdenDeFabricacion | Contiene todas las órdenes de fabricación |
| PP007 | VariableFormato | Contiene la configuracionde formatos para ciertos procesos y subprocesos |
| PP018 | Planta | Contiene las plantas de la organización con el código de identificación  |
| PP019 | RelacionEventoPuestoConfiguracion | Configuracion de precedencias de puestos por procesos y subprocesos |
| PP020 | ScriptPuestoConfiguracion | Relaciona operaciones a puesto logicos (SFCM29M) de determinado proceso y subproceso |
| PP032 | Planificacion | Relaciona linea con turno, articulo y cantidad |
| PP033 | Turno | Turnos por Unidad Organizativa con descripcion de fecha y horario de inicio y fin |
| PP042 | PAF | Contiene registros de PAF a traves de groovy (Version 2 de PAF) |
| PP043 | MatrizPolivalencia | Contiene registros en formato json de la asociacion de modelos y puestos con sus respectivas destrezas |
| PP046 | CabeceraParte | Dotacion |
| PP052 | AuthorizationForm | Contiene detalle de registros PAF |
| RH003 | Employee | Información de Empleados (legajo, nombre, apellido, etc) |
| RH011 | HorasPagadas |  |
| SFCM19M | LineaDeProduccion |  |
| SFCM20M | Configuracion (Linea) | Contiene la configuracion de las lineas |
| SFCM23M | DispositivoDeProduccion | Workstation para configuraciones de linea, impresoras, etc |
| SFCM29M | PuestoConfiguracion (Linea) | Configura puestos por linea (Ej: Validacion → HONTAI) |
| SFCM34M | RelacionPuestoConfiguracion | Relaciona un workstation con un puesto de la linea |
| SFCM38M | Configuracion (Proceso) | Configuración de proceso y subproceso |
| SFCM39M | PuestoConfiguracion (Proceso) | Relaciona un puesto logico (SFCM29M) con un proceso y subproceso(SFCM38M) |
| SISM05M | ValorSistema |  |
| SISM09M | ItemMenu |  |
| SISM10M | Link | Links a modulos del SIM |
| SISM11M | LinkRol | Roles |
| SISM33M | Archivo | Contiene diferentes tipos de archivos (Ej: imágenes) |
| SISM49M | Script |  |
| SS012 | Trabajo | Contiene los jobs |