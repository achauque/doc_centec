[<- Volver](README.md)

## Linux:

Instalamos llave PGP: 
```console
user@host:~$ wget -O - https://repo.fortinet.com/repo/6.4/ubuntu/DEB-GPG-KEY | sudo apt-key add -
```


Agregamos el repositorio en /etc/apt/sources.list 
```console
user@host:~$ sudo sh -c 'echo "deb [arch=amd64] https://repo.fortinet.com/repo/6.4/ubuntu/ /bionic multiverse" >> /etc/apt/sources.list'
```

Actualizamos el indice de paquetes de APT :  
```console
user@host:~$ sudo apt-get update
```

Instamos FortiClient:  
```console
user@host:~$ sudo apt install forticlient
```

Agregamos entradas al archivo /etc/hosts:  
```console
user@host:~$ sudo nano /etc/hosts
```

```
## MIRGOR ##
192.168.6.20 wiki.ad.mirgor.com.ar
192.168.5.85 git.ad.mirgor.com.ar
192.168.5.153 nexus.ad.mirgor.com.ar
192.168.10.50 rg.qa.sim.ad.mirgor.com.ar
192.168.10.61 vmrg1oraqa1.ad.mirgor.com.ar
192.168.10.29 servicedesk.ad.mirgor.com.ar
192.168.10.75 rg.sim.ad.mirgor.com.ar
192.168.10.86 simdbrg.ad.mirgor.com.ar
192.168.10.18 srvrg1dc1.ad.mirgor.com.ar
192.168.6.32 jira.ad.mirgor.com.ar
192.168.5.151 qa.emir.ad.mirgor.com.ar
192.168.6.32 jenkins.ad.mirgor.com.ar

```

Configuramos la conexiñon VPN:

![VPN1](misc/vpn1.png)

![VPN2](misc/vpn2.png)



Enlace de prueba:

http://rg.qa.sim.ad.mirgor.com.ar/sim/static/index.html


Referencia:

https://www.fortinet.com/support/product-downloads/linux


## Windows:

## Mac:
