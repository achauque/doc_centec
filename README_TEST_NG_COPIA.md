[<- Volver](README.md)

## Configurar en proyecto env:

PASO PREVIO

Configurar las propiedades antes en el proyecto ENV

![img](misc/env_1.png)

```console

application-test.properties
#spring.datasource.url=jdbc:oracle:thin:@srvdc1dkt01.AD.MIRGOR.COM.AR:32768:XE
esquema=SIMTEST
spring.datasource.url=jdbc:oracle:thin:@localhost:1521:xe
spring.datasource.username=${esquema}
spring.datasource.password=mir2pw

```

## TestNG - copiaDesdeEntornoCOPIA():

![img](misc/entornoCOPIA.png)


Click derecho sobre el método copiaDesdeEntornoCOPIA()

![img](misc/entornoCOPIA_1.png)


Solo se corre unos segundos y lo detenemos desde el botón cuadrado de stop.

Vamos a ver las configuraciones en Run Configurations, Click derecho del mouse o desde el panel RUN

![img](misc/entornoCOPIA_2.png)

Al haberlo corrido y detenido se genera el TestNG para CopiaEntornoRealTest.copiaDesdeEntornoCOPIA

![img](misc/entornoCOPIA_3.png)


## Configuramos los Arguments > VM arguments: 

-Dspring.profiles.active=test
-Dspring.config.location=/{ruta del proyecto}/emir2/env/properties/
-DLOG_PATH=/tmp/log/
-Dserver.port=8180
-Duser.country=AR -Duser.language=es

Listo, podemos darle al botón RUN

![img](misc/entornoCOPIA_4.png)

Demora unos minutos, luego deberíamos tener una salida en la consola:

PASSED: copiaDesdeEntornoCOPIA
========================================
Default test
Tests run: 1, Failures: 0, Skips: 0 
========================================
Default suite
Total tests run: 1, Failures: 0, Skips: 0

O desde la pestaña TestNG, en color verde y su detalle ok. O en rojo y su no ok.

![img](misc/entornoQA_5.png)


 

