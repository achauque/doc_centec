[<- Volver](README.md)

## Linux:
Requisitos: Ubuntu 20.04 LTS

Instalación vía apt: 

```console
user@host:~$ sudo apt install maven
```

Verificación versión: 

Es necesario verificar que la versión sea a la 3.6.~ y este apuntada a la versión 1.8 de jdk instalada.

```console
user@host:~$ mvn -version  
Apache Maven 3.6.3
Maven home: /usr/share/maven
Java version: 1.8.0_311, vendor: Oracle Corporation, runtime: /opt/jdk/jdk1.8.0_311/jre
Default locale: es_AR, platform encoding: UTF-8
OS name: "linux", version: "5.13.0-35-generic", arch: "amd64", family: "unix"
```

## Windows:

## Mac: