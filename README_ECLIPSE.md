[<- Volver](README.md)

## Linux:

Descargamos eclipse desde el sitio oficial:

https://www.eclipse.org/downloads/

Todo el proceso de instalación se llevara adelante asumiendo los directorios por defecto sugeridos por la aplicación.

Una vez descargado y descomprimido el instalador, procedemos a ejecutar el mismo:

![INSTALLER0](misc/eclipse_installer_0.png)

![INSTALLER1](misc/eclipse_installer_1.png)

![INSTALLER2](misc/eclipse_installer_2.png)

<br>

Una vez terminado el proceso de descarga el instalador no pedira un directorio para el espacio de trabajo. Por defecto esta alojado en el directorio personal del usuario (/home/usuario/eclipse-workspace)

![INSTALLER3](misc/eclipse_installer_3.png)


<br>

## Instalación de plugins necesarios para el proyecto:

Se necesitan 4 plugins para poder levantar el proyecto en eclipse:
* Web Developer Tools: se de herramientas para desarrollo web, entre ellas editores CSS, HTML y JSON.
* Maven: herramienta de gestión y contrucción de proyectos java.
* Groovy: soporte para lenguaje de scripting groovy.
* TestNG: framework para testing.


<br>
Todos los plugins pueden ser instalados desde el marketplace de eclipse. En todos los casos debemos aceptar las licencias propuestas por el instalador.

Las opcionaes seleccionadas en las imagenes son las que requiere el proyecto. Hay que tener especial cuidado con las opciones de groovy:


![MARKET_0](misc/menu_market.png)

## Web Developer Tools:

![INST_WEBT_0](misc/webtool_inst_0.png)

![INST_WEBT_1](misc/webtool_inst_1.png)

## Maven:

![INST_MAVEN_0](misc/maven_inst_0.png)

## Groovy:

![INST_GROOVY_0](misc/groovy_inst_0.png)

![INST_GROOVY_1](misc/groovy_inst_1.png)

## TestNG:

![INST_TESTNG_0](misc/testng_inst_0.png)

![INST_TESTNG_1](misc/testng_inst_1.png)


## Importar proyecto:

El ultimo paso es importar el proyecto clonado en el workspace de eclipse:

![IMPORT_0](misc/import_0.png)

En el boton "Browse" podremos navegar hasta la carpeta "emir2" donde se encuentra contenido todo proyecto.

Una vez seleccionada la carpeta necesitamos quitar de la importación el proyecto script


![IMPORT_1](misc/import_1.png)

Una vez realizadas estas tareas (con VPN conectada) se comenzara a recontruir el proyecto, y se podra ver el avance en la barra de progreso de eclipse. Esta tarea llevara unos minutos en realizarse.


## Windows:

## Mac: