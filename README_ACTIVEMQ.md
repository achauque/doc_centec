[<- Volver](README.md)

## Linux:

En una instalación fresca de Ubuntu no habra problema con este comando. En caso contrario solo se debe verificar que no se esten usando los puertos 61616 y 8161.

Tener en cuenta que este comando puede tardar algunos minutos dependiendo de la conexión y hardware. Ya que descarga el contenedor y lo despliega.

```console
user@host:~$ docker run -p 61616:61616 -p 8161:8161 rmohr/activemq
```

## Windows:

## Mac: